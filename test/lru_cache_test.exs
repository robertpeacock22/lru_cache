defmodule LRUCacheTest do
  use ExUnit.Case, async: true
  alias LRUCache

  setup do
    start_supervised!({LRUCache, [cache_size: 10]})

    :ok
  end

  test "initializes max size" do
    assert LRUCache.get_max_size() == 10
  end

  describe "LRUCache.get/1" do
    test "returns :not_found if provided key is not in cache" do
      add_five_camera_lenses_to_cache()

      assert LRUCache.get(:fisheye) == :not_found
    end

    test "gets a known stored value from the cache" do
      LRUCache.put(:wide, "14-30mm")

      assert LRUCache.get(:wide) == {:ok, "14-30mm"}
    end

    test "promotes the fetched value to the head of the queue" do
      add_five_camera_lenses_to_cache()

      assert LRUCache.get(:telephoto) == {:ok, "200-500mm"}

      assert LRUCache.inspect().contents ==
               [
                 %{key: :telephoto, value: "200-500mm"},
                 %{key: :macro, value: "105mm"},
                 %{key: :prime, value: "50mm"},
                 %{key: :wide, value: "14-30mm"},
                 %{key: :kit, value: "24-70mm"}
               ]
    end
  end

  describe "LRUCache.put/2" do
    test "puts a single value into the cache" do
      LRUCache.put(:telephoto, "200-500mm")

      assert LRUCache.inspect().contents == [%{key: :telephoto, value: "200-500mm"}]
    end

    test "puts a duplicate value into the cache only once" do
      LRUCache.put(:kit, "24-70mm")
      LRUCache.put(:kit, "24-70mm")
      LRUCache.put(:kit, "24-70mm")

      assert LRUCache.inspect().contents == [%{key: :kit, value: "24-70mm"}]
    end

    test "puts multiple values in the cache while retaining order" do
      LRUCache.put(:kit, "24-70mm")
      LRUCache.put(:telephoto, "200-500mm")
      LRUCache.put(:wide, "14-30mm")

      assert LRUCache.inspect().contents ==
               [
                 %{key: :wide, value: "14-30mm"},
                 %{key: :telephoto, value: "200-500mm"},
                 %{key: :kit, value: "24-70mm"}
               ]
    end

    test "evicts least-recently-used values when putting new value" do
      LRUCache.resize(3)

      LRUCache.put(:kit, "24-70mm")
      LRUCache.put(:telephoto, "200-500mm")
      LRUCache.put(:wide, "14-30mm")
      LRUCache.put(:prime, "50mm")

      assert LRUCache.inspect().contents ==
               [
                 %{key: :prime, value: "50mm"},
                 %{key: :wide, value: "14-30mm"},
                 %{key: :telephoto, value: "200-500mm"}
               ]
    end

    test "puts different value types into the cache" do
      LRUCache.put(:brand, :nikon)
      LRUCache.put(:model, "Z6")
      LRUCache.put(:card_slots, 1)

      LRUCache.put(:af_modes, %{
        af_s: "Single",
        af_c: "Continuous",
        full: "Full-time"
      })

      assert LRUCache.inspect().contents ==
               [
                 %{
                   key: :af_modes,
                   value: %{
                     af_s: "Single",
                     af_c: "Continuous",
                     full: "Full-time"
                   }
                 },
                 %{key: :card_slots, value: 1},
                 %{key: :model, value: "Z6"},
                 %{key: :brand, value: :nikon}
               ]
    end
  end

  describe "LRUCache.remove/1" do
    test "removes a single value from the cache" do
      LRUCache.put(:telephoto, "200-500mm")

      assert LRUCache.inspect().contents == [%{key: :telephoto, value: "200-500mm"}]

      LRUCache.remove(:telephoto)

      assert LRUCache.inspect().contents == []
    end

    test "retains order-of-use when removing from a populated cache" do
      add_five_camera_lenses_to_cache()

      LRUCache.remove(:telephoto)

      assert LRUCache.inspect().contents ==
               [
                 %{key: :macro, value: "105mm"},
                 %{key: :prime, value: "50mm"},
                 %{key: :wide, value: "14-30mm"},
                 %{key: :kit, value: "24-70mm"}
               ]
    end

    test "gracefully handles removal request of non-existent key" do
      LRUCache.remove(:noct)

      assert LRUCache.inspect().contents == []
    end
  end

  describe "LRUCache.resize/1" do
    test "can increase cache size" do
      assert LRUCache.get_max_size() == 10

      LRUCache.resize(15)

      assert LRUCache.get_max_size() == 15
    end

    test "existing contents are unaffected when increasing cache size" do
      add_five_camera_lenses_to_cache()

      LRUCache.resize(20)

      assert LRUCache.inspect().contents == initial_five_camera_lenses()
    end

    test "can decrease cache size" do
      assert LRUCache.get_max_size() == 10

      LRUCache.resize(3)

      assert LRUCache.get_max_size() == 3
    end

    test "existing contents are unaffected when cache size is decreased but still fits current contents" do
      add_five_camera_lenses_to_cache()

      LRUCache.resize(5)

      assert LRUCache.inspect().contents == initial_five_camera_lenses()
    end

    test "evicts contents in LRU order when size is decreased to smaller than current contents" do
      add_five_camera_lenses_to_cache()

      LRUCache.resize(3)

      assert LRUCache.inspect().contents ==
               [
                 %{key: :macro, value: "105mm"},
                 %{key: :prime, value: "50mm"},
                 %{key: :wide, value: "14-30mm"}
               ]
    end

    test "evicts all items from cache when resized to 0" do
      add_five_camera_lenses_to_cache()

      LRUCache.resize(0)

      assert LRUCache.inspect().contents == []
    end
  end

  defp add_five_camera_lenses_to_cache() do
    LRUCache.put(:kit, "24-70mm")
    LRUCache.put(:telephoto, "200-500mm")
    LRUCache.put(:wide, "14-30mm")
    LRUCache.put(:prime, "50mm")
    LRUCache.put(:macro, "105mm")
  end

  defp initial_five_camera_lenses() do
    [
      %{key: :macro, value: "105mm"},
      %{key: :prime, value: "50mm"},
      %{key: :wide, value: "14-30mm"},
      %{key: :telephoto, value: "200-500mm"},
      %{key: :kit, value: "24-70mm"}
    ]
  end
end
