defmodule LRUCache.Application do
  @moduledoc false
  use Application

  def start(_type, _args),
    do: Supervisor.start_link(children(), opts())

  defp children do
    case Application.get_env(:lru_cache, :env) do
      :test -> []
      _ -> [{LRUCache, cache_size: LRUCache.default_size()}, LRUCache.Endpoint]
    end
  end

  defp opts do
    [
      strategy: :one_for_one,
      name: LRUCache.Supervisor
    ]
  end
end
