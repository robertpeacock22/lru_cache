defmodule LRUCache.Router do
  @moduledoc false
  use Plug.Router
  use Plug.ErrorHandler
  alias LRUCache

  plug(:match)
  plug(:dispatch)

  get "/" do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(LRUCache.inspect()))
  end

  get "/item/:key" do
    value = LRUCache.get(String.to_atom(key))

    return =
      if value == :not_found do
        "No value stored for #{key}"
      else
        {:ok, stored_value} = value

        stored_value
      end

    send_resp(conn, 200, return)
  end

  put "/item" do
    %{"key" => key, "value" => value} = conn.body_params
    LRUCache.put(String.to_atom(key), value)
    {:ok, result} = Poison.encode(LRUCache.inspect())
    send_resp(conn, 200, result)
  end

  delete "/item" do
    %{"key" => key} = conn.body_params

    result =
      key
      |> String.to_atom()
      |> LRUCache.remove()
      |> Atom.to_string()

    send_resp(conn, 200, result)
  end

  patch "/size" do
    %{"size" => size} = conn.body_params
    LRUCache.resize(size)
    {:ok, result} = Poison.encode(LRUCache.inspect())
    send_resp(conn, 200, result)
  end

  def handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
    send_resp(conn, conn.status, "Something went wrong")
  end
end
