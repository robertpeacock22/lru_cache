# LruCache

A dynamically-resizeable least-recently-used cache written in Elixir.

`iex -S mix` - Run locally (refer to module docs).

`mix test` - Run unit tests.

`mix coveralls` - Run code coverage tool.

## API

Run LRUCache as an API by using `mix run --no-halt`. The API will then be available on **localhost:4000**.

`/` - Inspect the current contents of the cache, as well as details of the cache size (max and current)

`/item` (PUT) - PUT a new value in the cache. The value is promoted to the head of the recency list.
```
{
  "key": "key_for_string",
  "value": "This is a string value"
}

{
  "key": "key_for_boolean",
  "value": true
}

{
  "key": "key_for_integer",
  "value": 1234
}

{
  "key": "key_for_float",
  "value": 1234.5
}

{
  "key": "key_for_object",
  "value": {"whatever": "You can put an object in the cache!"}
}
```

`/item/:key` (GET) - Get the value stored for the provided `key`. The value is promoted to the head of the recency list.

`/item` (DELETE) - Remove the value associated with the given key from the cache if it exists.
```
{
  "key": "key_to_remove"
}
```

`/size` (PATCH) - Change the size of the cache. If the new cache size is smaller than the current cache contents, some contents will be evicted, in order of least-recently-used.
```
{
    "size": 20
}
```
