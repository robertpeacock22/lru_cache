defmodule LRUCache do
  @moduledoc """
  A key-value cache that evicts items based on recency of use;
  the least-recently-used item is always the first to be evicted.
  """

  use GenServer
  @name LRUCache
  @default_size 10

  defstruct max_size: 0, cache_order: [], cache_values: %{}

  # Client API

  @doc false
  def default_size(), do: @default_size

  @doc false
  def child_spec(opts) do
    %{id: LRUCache, start: {__MODULE__, :start_link, [opts]}, type: :supervisor}
  end

  @doc false
  def start_link(opts) do
    GenServer.start_link(__MODULE__, Keyword.get(opts, :cache_size), opts ++ [name: @name])
  end

  @doc """
  Returns the current maximum size of the cache (defaults to #{@default_size})

  If the number of items in the cache exceeds the maximum size – whether through
  adding new items or through reducing the size – items are evicted until this
  constraint is met.
  """
  def get_max_size() do
    GenServer.call(@name, :get_max_size)
  end

  @doc """
  Caches `value` under the given `key`

  This value is moved to the head of the queue.

  ## Parameters

    - key: An atom that uniquely identifies this pairing
    - value: The data to cache

  """
  def put(key, value) when is_atom(key) do
    GenServer.call(@name, {:put, key: key, value: value})
  end

  @doc """
  Returns the value cached under `key`, in the form `{:ok, value}`

  This value is moved to the head of the queue.

  If no such value exists, `:not_found` is returned instead.

  ## Parameters

    - key: The key to find the associated value for
  """
  def get(key) when is_atom(key) do
    GenServer.call(@name, {:get, key: key})
  end

  @doc """
  Removes the `key` and its associated value from the cache

  ## Parameters

    - key: The key to be removed
  """
  def remove(key) when is_atom(key) do
    GenServer.call(@name, {:remove, key: key})
  end

  @doc """
  Resizes the cache such that it can fit `size` elements

  If the new size is greater than or equal to the current number of cached elements,
  current values are unaffected. If the new size is smaller than the current number of
  cached elements, however, elements will be evicted in a last-used-first-out manner.

  ## Parameters

    - size: a non-negative integer describing the new size of the cache
  """
  def resize(size) when is_integer(size) and size >= 0 do
    GenServer.call(@name, {:resize, size: size})
  end

  @doc """
  Returns the entire contents of the cache

  The returned contents are ordered from most-recently-used to least-recently-used
  (where "used" means either added or accessed).
  """
  def inspect() do
    GenServer.call(@name, :inspect)
  end

  # Server Callbacks

  @impl true
  def init(cache_size) do
    {:ok, %LRUCache{max_size: cache_size, cache_order: [], cache_values: %{}}}
  end

  @impl true
  def handle_call(:get_max_size, _from, %LRUCache{} = cache) do
    {:reply, cache.max_size, cache}
  end

  @impl true
  def handle_call({:get, [key: key]}, _from, %LRUCache{cache_values: cache_values} = cache) do
    if Map.has_key?(cache_values, key) do
      cache_with_new_order = cache |> update_order(key)
      found_value = Map.get(cache_values, key)
      {:reply, {:ok, found_value}, cache_with_new_order}
    else
      {:reply, :not_found, cache}
    end
  end

  @impl true
  def handle_call({:put, [key: key, value: value]}, _from, %LRUCache{} = cache) do
    new_cache =
      cache
      |> update_values(key, value)
      |> update_order(key)
      |> trigger_eviction()

    {:reply, :ok, new_cache}
  end

  @impl true
  def handle_call(
        :inspect,
        _from,
        %LRUCache{max_size: max_size, cache_order: cache_order, cache_values: cache_values} =
          cache
      ) do
    ordered_cache_contents =
      cache_order
      |> Enum.reduce([], fn key, acc ->
        value = Map.get(cache_values, key)
        acc ++ [%{key: key, value: value}]
      end)

    reply = %{
      contents: ordered_cache_contents,
      max_size: max_size,
      available: max_size - length(ordered_cache_contents)
    }

    {:reply, reply, cache}
  end

  @impl true
  def handle_call({:remove, [key: key_to_remove]}, _from, %LRUCache{} = cache) do
    new_cache =
      cache
      |> remove_from_order(key_to_remove)
      |> remove_from_values(key_to_remove)

    {:reply, :ok, new_cache}
  end

  @impl true
  def handle_call({:resize, [size: new_size]}, _from, %LRUCache{} = cache) do
    new_cache =
      cache
      |> set_size(new_size)
      |> trigger_eviction()

    {:reply, :ok, new_cache}
  end

  # Helper Functions

  defp update_values(%LRUCache{cache_values: cache_values} = cache, key, value)
       when is_atom(key) do
    %LRUCache{cache | cache_values: Map.put(cache_values, key, value)}
  end

  defp update_order(%LRUCache{cache_order: cache_order} = cache, key) when is_atom(key) do
    new_cache_order = List.delete(cache_order, key)
    %LRUCache{cache | cache_order: [key] ++ new_cache_order}
  end

  defp remove_from_values(%LRUCache{cache_values: cache_values} = cache, key) when is_atom(key) do
    %LRUCache{cache | cache_values: Map.delete(cache_values, key)}
  end

  defp remove_from_order(%LRUCache{cache_order: cache_order} = cache, key) when is_atom(key) do
    %LRUCache{cache | cache_order: List.delete(cache_order, key)}
  end

  defp trigger_eviction(
         %LRUCache{max_size: max_size, cache_order: cache_order, cache_values: cache_values} =
           cache
       )
       when length(cache_order) > max_size do
    # Cache is over-full. Time to evict.
    [last | rest] = Enum.reverse(cache_order)
    new_cache_values = Map.delete(cache_values, last)
    new_cache_order = Enum.reverse(rest)

    new_cache = %LRUCache{
      cache
      | cache_order: new_cache_order,
        cache_values: new_cache_values
    }

    trigger_eviction(new_cache)
  end

  defp trigger_eviction(%LRUCache{} = cache) do
    # Cache is not over-full. No-op.
    cache
  end

  defp set_size(%LRUCache{} = cache, new_size) when is_integer(new_size) and new_size >= 0 do
    %LRUCache{cache | max_size: new_size}
    |> trigger_eviction()
  end
end
